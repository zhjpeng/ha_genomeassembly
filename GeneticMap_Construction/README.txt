1. 首先，创建遗传图谱构建的主文件夹 r_LinkageMap

2. 准备参考基因组，亲本和子代测序文件，并制作谱系文件RH.pedigree
    RH.pedigree文件第一行为家系名称，第二行为样品名称，第三行为雄性亲本，第四行为雌性亲本，第五行为样品性别（不知道可以填写为0），第六行为表型（不知道可以填写为0）

3. 运行bash脚本SmartRun.sh

    关于Lep-map3:
    ParentCall2：以亲本数据为依据，计算各个体的基因型对应的分离比等数据，我们简称其结果文件为call。
    Filtering2：该模块是对数据进行过滤的模块，默认按二倍体输出（1和2）。
                该模块主要过滤掉偏分离位点以及缺失率太高的位点，需要注意的一点是，软件默认的是按CP或者F2作图群体过滤，如果作图群体为RIL群体，软件计算时也会使用F2群体标准进行过滤，
                该条件下，RIL群体中所有的个体基因型的分离比不符合孟德尔遗传定律，会全部处理成缺失。因此RIL群体作图时数据不经过过滤，直接进行图谱绘制。
    SeparateChromosomes2：该模块主要进行作图计算（和大多数软件一样，Lep-map3也是采用pair-wise LOD scores进行连锁群的划分），
                          其功能类似于分群，主要是对call文件的位点根据设定的LOD和theta阈值分组，划分为不同的LGs，之后统计每个LGs中符合要求的位点个数即信息
    JoinSingles2All：① 该模块是用来加密连锁群的，通过重新计算未划分到连锁群的标记和已划分到连锁群的标记的LOD值，降低划分连锁群的LOD阈值，再次将这些未划分的标记分配到合适的连锁群中
                     ② 对于鳞翅目（果蝇）等某性别不发生遗传重组的情况来说，这一步最为关键。首先在SeparateChromosomes2时屏蔽重组性别的有效分子标记，即利用不发生重组性别的有效分子标记
                       来划分连锁群，然后屏蔽不发生重组的性别的有效分子标记，将发生重组性别的有效信息分子标记添加到各个连锁群上
    OrderMarkers2：对上一步输出的、符合要求的所有位点进行遗传距离的计算，最终确定各位点之间的毗邻关系及遗传距离
    
4. 解读Lep-map3的输出结果
   ParentCall2的结果文件：
   第一行为生成该文件的命令，如java ParentCall2 data=RH.pedigree vcfFile=50NoPa_Missing.vcf removeNonInformative=1
   第二至第七行及谱系文件RH.pedigree的内容，同时也是第七行之后内容的表头信息
   第八行及以后，为不同位点likelihoods的信息，前两列分别为染色体，物理位置；
     后面的每列对应一个样本的likelihoods，每个likelihood有10个数据信息，分别代表SNP基因型的10种可能形式：AA,AC,AG,AT,CC,CG,CT,GG,GT和TT。
     当然如果是非SNP类型标记也是适用的，只要对应的转换成能区分的信息就可以，例如纯合显性基因型信息可以用AA，杂合信息用AT，纯合隐性用TT等
   
   Filtering2结果文件：
   第一行为生成该文件的命令，如java Filtering data=- dataTolerance=0.001
   本质上它的结果文件和ParentCall2类似，只是修改了偏分离位点以及不符合孟德尔遗传定律的位点标记likelihoods值，以示缺失（每个likelihood的10个数据信息标志为1.0？）。
   
   SeparateChromosomes2结果文件：
   第一列为生成该文件的命令，如java SeparateChromosomes2  data=- lodLimit=15 sizeLimit=100 distortionLod=1 numThreads=47 femaleTheta=0 informativeMask=2
   第二行及以后每一行只有一个数字，这里每一行对应相应行位置（去除表头信息），可以使用linux里的paste命令进行合并，而这里的数字代表这个标记分配到的连锁群编号，0表示未分配
   
   JoinSingles2All结果文件
   第一行为生成该文件的命令，如java JoinSingles2  map=map15.txt data=- lodLimit=10 iterate=1 numThreads=47
   本质上它的结果文件和SeparateChromosomes2类似，只是修改在该环节添加上连锁群位点的信息，如29"\t"20.714114425228786"\t"2"\t"16.873487690367206；
   而上环节已经在遗传图谱上的标记不会发生改变
   
   OrderMarkers2结果文件
   第一行为生成该文件的命令，如java OrderMarkers2 data=- map=errp_map15_Lod10.txt chromosome=10 numThreads=47 identicalLimit=0.01 informativeMask=1 recombination2=0 sexAveraged=0 numMergeIterations=5 randSeed=583791997960363366
   第二行为连锁群的编号（对应于JoinSingles2All结果文件中的编号）和likelihood值
   第三行为表头信息，marker_number"\t"male_position"\t"female_position ( 0.0 )[phased data]
   
   每一个分群对应两张图谱，一个是male，一个是female
   第一列是位点编号（对应于JoinSingles2All结果文件中的编号）
   第二列为male遗传距离
   第三列为female遗传距离，其中很多位点对应的遗传距离，表明这些位点是一个bin，如果图谱中gap比较大，可以尝试减少重组率的值来缩小差距（initRecombination默认为0.05）
   
   正常情况下，我们可以生成三种图谱。即单性图谱，雄性/雌性图谱；双性平均图谱（设置sexAveraged=1）
   
   对于棉铃虫，我们具体的命令在SmartRun.sh中详细列出
   
5. 数据整合
   
   
   
   