#!/usr/bin/python

# generate input data for ggplot2 to plot links between genetic map and physical map
# USAGE: python2 a-LepmapTreat.py ../10Filtering2/data.call_f.gz chrlen.txt 2_1
# for K in {1..31};do python2 a-LepmapTreat.py ../10Filtering2/data.call_f.gz chrlen.txt ${K}_1 >> Plot_linkage.txt;done

import sys
import gzip

## load data of physical map
count = 0
PM_dic = {}
PM_list = []

for line in gzip.open(sys.argv[1]):
    if line.startswith("#") or line.startswith("CHR"):
        continue
    else:
        line1 = line.strip().split("\t")
        count = count + 1
        PM_list.append(line1[0])
        PM_list.append(line1[1])
        PM_dic[count] = PM_list
        PM_list = []

# load thresholds cM of remove
trim_dic = {}
trim_list = []
for thds in open(sys.argv[2]):
    if thds.strip().startswith("#"):
        continue
    else:
        thds1 = thds.strip().split("\t")
        trim_list.append(thds1[5])
        trim_list.append(thds1[6])
        trim_list.append(thds1[7])
        trim_dic[thds1[2].split("lg")[1]] = trim_list
        trim_list = []

## load data of linkage map
LG_dic = {}
LG_list = []
LG = 0

for item in open(sys.argv[3]):
    item1 = item.strip()
    if item1.startswith("#*** LG"):
        LG = item1.split(" ")[3].strip()
    elif item1.startswith("#"):
        continue
    else:
        item2 = item1.split("\t")
        if float(item2[1]) < float(trim_dic[LG][0]) or float(item2[1]) > float(trim_dic[LG][1]):
            continue
        else:
            lg_trim = float(item2[1]) - float(trim_dic[LG][0])
            if LG=="29" or LG=="14" or LG=="21" or LG=="27" or LG=="13" or LG=="24" or LG=="11" or LG=="7" or LG=="19" or LG=="18" or LG=="20" or LG=="31" or LG=="28":
                print PM_dic[int(item2[0])][0].split("chr")[1] + "\t" + trim_dic[LG][2] + "\t" + PM_dic[int(item2[0])][1] + "\t" + LG + "\t" + str(float(trim_dic[LG][1])-float(trim_dic[LG][0])-lg_trim)
            else:
                print PM_dic[int(item2[0])][0].split("chr")[1] + "\t" + trim_dic[LG][2] + "\t" + PM_dic[int(item2[0])][1] + "\t" + LG + "\t" + str(lg_trim)