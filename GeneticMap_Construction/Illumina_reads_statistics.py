import sys
import gzip

# for K in $(ls | awk -F '_' '/fq.gz$/{print $1}' | sort| uniq)
# do
#     python3 Illumina_reads_statistics.py ${K}_paired_R1.fq.gz ${K}_paired_R2.fq.gz >> geneticMapIdividualData.txt
# done

def FastqParser(fastq):
    totol_row = 0
    seq_row = 0
    seq = ""

    for line in gzip.open(sys.argv[1]):
        line = line.strip()
        totol_row = totol_row + 1
        if totol_row % 4 == 2:
            seq_row = seq_row + 1
            seq = seq + line.decode()

    G = seq.count("G")
    C = seq.count("C")
    L = len(seq)
    return seq_row, G, C, L

file1 = FastqParser(sys.argv[1])
file2 = FastqParser(sys.argv[2])

seq_row = int(file1[0]) + int(file2[0])
G = int(file1[1]) + int(file2[1])
C = int(file1[2]) + int(file2[2])
L = int(file1[3]) + int(file2[3])

GC_content = (G + C)/L
print(sys.argv[1], sys.argv[2], str(GC_content), str(seq_row), str(L), sep = "\t")
