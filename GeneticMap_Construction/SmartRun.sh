#!/bin/bash

## Defines File Pathways
Main=$(pwd)
TempPath=${Main}/00-PreData
RefPath=${Main}/10-RefSeq
RawPath=${Main}/20-RawData
#CleanPath=/home/newsdd/store/geneticmap/0bCleandata
BamPath=${Main}/40-BamFiles
GvcfPath=${Main}/50-GvcfFiles
VcfPath=${Main}/60-VcfFiles
LepPath=${Main}/70-LepMap

## Defines Software Pathways
GAT=/home/dell/biosoft
TRI=${GAT}/trimmomatic/Trimmomatic-0.36
PIC=${GAT}/picardtools/picard-tools-1.119
LEP=${GAT}/Lep-MAP3/bin

## Defines Name
RefPreName=HaChrv2
PedAllName=RH.pedigree

## Defines Parameters
ThreadRun=47

# Create necessary files
mkdir -p ${RefPath} ${BamPath} ${GvcfPath} ${VcfPath} ${LepPath}

cp ${TempPath}/${RefPreName}.fa ${RefPath}

# index genome
bwa index -a bwtsw -p ${RefPath}/${RefPreName} ${RefPath}/${RefPreName}.fa && \
java -jar ${PIC}/CreateSequenceDictionary.jar \
          REFERENCE=${RefPath}/${RefPreName}.fa \
          OUTPUT=${RefPath}/${RefPreName}.dict &&\

samtools faidx ${RefPath}/${RefPreName}.fa && \

echo -e "\n\n\n*******genome is indexed successfully*******\n\n\n"

## Reads Alignment
cd ${BamPath} && mkdir 00temp

for K in $(ls ${CleanPath} | grep ".fq.gz$" | awk -F '_' '{print $1}'|sort |uniq)
do
    bwa mem -R "@RG\tID:foo\tSM:${K}\tLB:library1" \
            -t ${ThreadRun}  \
            -M ${RefPath}/${RefPreName} \
            ${CleanPath}/${K}_paired_R1.fq.gz \
            ${CleanPath}/${K}_paired_R2.fq.gz \
            -o ./00temp/${K}.sam && \

    echo -e "\n******* Reads of ${K} have been mapped to reference genome successfully *******\n" &&

    java -jar ${PIC}/SortSam.jar \
            INPUT=./00temp/${K}.sam  \
            OUTPUT=./00temp/${K}.bam \
            SORT_ORDER=coordinate && rm ./00temp/${K}.sam && \
    # remove multi-mapping reads
    samtools view -b -q 10 ./00temp/${K}.bam > ./00temp/${K}.filter.bam && \

    echo -e "\n******* Sam file of ${K} have been transferred to bam format and indexed successfully*******\n" && \

    # mark duplicated reads
    java -jar ${PIC}/MarkDuplicates.jar \
            INPUT=./00temp/${K}.filter.bam \
            OUTPUT=./${K}_dedup.bam \
            METRICS_FILE=./${K}_dedup.matrics.txt && rm ./00temp/${K}.bam && rm ./00temp/${K}.filter.bam && \

    echo -e "\n******* Duplications of ${K} have been marked successfully*******\n"  && \

    # create index for bam file
    samtools index ./${K}_dedup.bam -@ ${ThreadRun} && \

    echo -e "\n******* Duplicated bams of ${K} have been indexed successfully*******\n" &

    sleep 10m
done

# start to run multi-commands  per 15m
# it will enter following commands until existing commands complete
wait

echo -e "\n****** Reads Alignment Stage is OK******\n"

## call variants by GATK
cd ${GvcfPath}

GVCFFILE=""
for K in $(ls ${BamPath} | grep ".bam$" | awk -F '_' '{print $1}'|sort |uniq)
do
    java -jar ${GAT}/GenomeAnalysisTK.jar \
         -T HaplotypeCaller \
         -R ${RefPath}/${RefPreName}.fa \
         -I ${BamPath}/${K}_dedup.bam \
         --emitRefConfidence GVCF \
         -o ./${K}.g.vcf \
         -stand_call_conf 10 \
         -A StrandAlleleCountsBySample \
         -nct ${ThreadRun}

    GVCFFILE=${GVCFFILE}"--variant ${GvcfPath}/${K}.g.vcf "
done

cd ${VcfPath} && mkdir 00temp

# genotype
java -jar ${GAT}/GenomeAnalysisTK.jar \
     -T GenotypeGVCFs \
     ${GVCFFILE} \
     -R ${RefPath}/${RefPreName}.fa \
     -o ./00temp/01Basic_region.vcf \
     -nt ${ThreadRun}

echo -e "\n******* Variants Calling Stage is OK *******\n"

# select SNPs
java -jar ${GAT}/GenomeAnalysisTK.jar \
    -T SelectVariants \
    -R ${RefPath}/${RefPreName}.fa \
    -V ${VcfPath}/00temp/01Basic_region.vcf \
    -o ${VcfPath}/20SelectSNP.vcf \
    -selectType SNP \
    -nt ${ThreadRun}

# hard filters of SNPs
java -jar ${GAT}/GenomeAnalysisTK.jar \
    -T VariantFiltration \
    -R ${RefPath}/${RefPreName}.fa \
    --variant ${VcfPath}/20SelectSNP.vcf \
    -o ${VcfPath}/30HardFilter.vcf \
    --filterName "S" \
    --filterExpression "QD < 2.0 || FS > 60.0 || MQ < 30.0 || HaplotypeScore > 13.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0" 2>WarningSNPFilter.txt

# criterions of populations
vcftools --vcf ${VcfPath}/30HardFilter.vcf \
           --remove-filtered-all \
           --max-alleles 2 \
           --max-alleles 2 \
           --minGQ 10 \
           --max-missing 0.7 \
           --maf 0.03 \
           --recode \
           --out ${VcfPath}/40HighQuality

# remove sites missing in either father or mother
python ${TempPath}/Pa_MissFilter.py ${VcfPath}/40HighQuality.recode.vcf > ${VcfPath}/50NoPa_Missing.vcf

echo -e "\n******* Variants Calling Stage is OK *******\n"

# create genetic map by Lep-map3
mkdir -p ${LepPath}/00ParentCall2 && cp ${TempPath}/${PedAllName} ${LepPath}/00ParentCall2/

# load vcf file of family and correct potential genotype errors or missings
java -cp ${LEP} ParentCall2 \
                data=${LepPath}/00ParentCall2/${PedAllName} \
                vcfFile=${VcfPath}/50NoPa_Missing.vcf \
                removeNonInformative=1 | gzip>${LepPath}/00ParentCall2/data.call.gz

java -cp ${LEP} ParentCall2 \
                data=${LepPath}/00ParentCall2/${PedAllName} \
                vcfFile=${VcfPath}/50NoPa_Missing.vcf \
                removeNonInformative=1 | gzip>${LepPath}/00ParentCall2/data.call_f.gz

# filter sites
mkdir -p ${LepPath}/10Filtering2
zcat ${LepPath}/00ParentCall2/data.call.gz | java -cp ${LEP} Filtering2 \
                                      data=- \
                                      dataTolerance=0.001 | gzip > ${LepPath}/10Filtering2/data.call_f.gz

#mkdir -p ${LepPath}/20SeparateChromosomes2
for K in 5 10 15 20 25 30
do
    zcat ${LepPath}/00ParentCall2/data.call_f.gz| java -cp ${LEP} SeparateChromosomes2 \
                                           data=- \
                                           lodLimit=${K} \
                                           sizeLimit=100 \
                                           distortionLod=1 \
                                           numThreads=${ThreadRun} \
                                           femaleTheta=0 \
                                           informativeMask=2 > ${LepPath}/20SeparateChromosomes2/map${K}.txt
done

mkdir -p ${LepPath}/30JoinSingles2All
zcat ${LepPath}/00ParentCall2/data.call_f.gz | java -cp ${LEP} JoinSingles2All \
                                                     map=${LepPath}/20SeparateChromosomes2/map15.txt \
                                                     data=- \
                                                     lodLimit=10 \
                                                     iterate=1 \
                                                     numThreads=${ThreadRun}>${LepPath}/30JoinSingles2All/map15_Lod10_iterated.txt

mkdir -p ${LepPath}/40OrderMarkers2/
for Q in $(seq 1 31)
do
    zcat ${LepPath}/00ParentCall2/data.call_f.gz | java -cp ${LEP} OrderMarkers2 \
                      data=- \
                      map=${LepPath}/30JoinSingles2All/errp_map15_Lod10.txt \
                      chromosome=${Q} \
                      numThreads=${ThreadRun} \
                      identicalLimit=0.01 \
                      informativeMask=1 \
                      recombination2=0 \
                      sexAveraged=0 \
                      numMergeIterations=5 >${LepPath}/40OrderMarkers2/${Q}_1
done