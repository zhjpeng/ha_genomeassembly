# python3 mcscanx2circos.py 4-mcscanx/Se-vs-Ha/Se-vs-Ha.gff 4-mcscanx/Se-vs-Ha/Se-vs-Ha.collinearity > 4-mcscanx/Se-vs-Ha/Se-vs-Ha.link

import sys

Name2Info = {}

for item in open(sys.argv[1]):
    item = item.strip()
    item1 = item.split("\t")

    geneInfo = []
    geneName = item1[1]
    geneInfo.append(item1[0])
    geneInfo.append(item1[2])
    geneInfo.append(item1[3])

    Name2Info[geneName] = geneInfo

for line in open(sys.argv[2]):
    line = line.strip()
    if line.startswith('#'):
        continue
    else:
        line1 = line.split("\t")
        Agene = line1[1]
        Bgene = line1[2]

        Acont = Name2Info[Agene]
        Bcont = Name2Info[Bgene]

        print(*Acont, *Bcont, sep="\t")
#        print(x) for x in Acont,ser="\t"
