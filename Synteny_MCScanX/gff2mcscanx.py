#!/usr/bin/env python
# -*- coding: utf-8 -*-

# convert gff3 file to meet MCScanX requirement of gene locations
# python MCScanX-gff3-simplify4.py Bm.gff > Bo-mcscanx-gff.txt
# python MCScanX-gff3-simplify4.py Haa.gff > Ha-mcscanx-gff.txt

import sys

for line in open(sys.argv[1]):
    if line.startswith("#"):
        continue
    else:
        item = line.strip().split("\t")
        if item[2] == "mRNA":
            chr_ID = item[0]
            pos_start = item[3]
            pos_end = item[4]
            gene_name = item[8].split("ID=")[1].split(";")[0]

            print chr_ID + "\t" + gene_name + "\t" + pos_start + "\t" + pos_end
