# python SortChrID.py specified.order genome.fa

# this script can sort chromosomes in the specified order

## the second column indicates revised chromosome id, it in general is fixed
## the first column is the messy chromosme id in above genome.fa corresponding to revised chromosome id
## the third column is symbol whether need to change sequence orient, i.e. "+" indicates to keep raw orient, and "-" indicates to change raw orient
# chr24   chr1    +
# chr14   chr2    -
# chr9    chr3    -
# chr7    chr4    +
# chr21   chr5    +
# chr4    chr6    -
# chr26   chr7    +
# chr2    chr8    -
# chr22   chr9    +

import sys
pair_chrid = {}
messy_id = []
orient_correction = []

load_genome = {}
chromosome_id = ''
chromoeome_seqence = ''

# define function for complementation
def dna_complement(sequence):
    sequence = sequence.replace('a', 't')
    sequence = sequence.replace('t', 'a')
    sequence = sequence.replace('c', 'g')
    sequence = sequence.replace('g', 'c')
    sequence = sequence.replace('A', 'T')
    sequence = sequence.replace('T', 'A')
    sequence = sequence.replace('C', 'G')
    sequence = sequence.replace('G', 'C')
    return sequence

for item in open(sys.argv[1]):
    item = item.strip()
    item1 = item.split("\t")
    pair_chrid[item1[0]] = item1[1]
    messy_id.append(item1[0])
    orient_correction.append(item1[2])

for line in open(sys.argv[2]):
    line = line.strip()
    if line.startswith('>') and chromoeome_seqence != '':
        load_genome[chromosome_id] = chromoeome_seqence
        line1 = line.split('>')
        chromosome_id = line1[1].split(' ')[0]
        chromoeome_seqence = ''
    elif line.startswith('>'):
        line = line.strip()
        line1 = line.split('>')
        chromosome_id = line1[1].split(' ')[0]
    else:
        chromoeome_seqence = chromoeome_seqence + line

# add last chromosome to dictionary
load_genome[chromosome_id] = chromoeome_seqence

for num, item in enumerate(messy_id):
    if orient_correction[num] == "+":
        chromoeome_seqence = load_genome[item]
        print(">", pair_chrid[item], sep="")
        i = 0
        while i < len(chromoeome_seqence):
            print(chromoeome_seqence[i:i+80])
            i = i + 80
    if orient_correction[num] == "-":
        chromoeome_seqence = dna_complement(load_genome[item][::-1])
        print(">", pair_chrid[item], sep="")
        i = 0
        while i < len(chromoeome_seqence):
            print(chromoeome_seqence[i:i+80])
            i = i + 80
