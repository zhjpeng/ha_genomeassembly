#!/usr/bin/env python
# -*- coding: utf-8 -*-

# python Fasta2Karyotype.py Haa.fa |grep -v "Haa_chrUn" > Karyotype.txt

import sys

Seq = ""
SeqID = ""

for Line in open(sys.argv[1]):
    Line = Line.strip()
    if Line[0] == ">" and Seq == "":
        SeqID = Line.split('>')[1]
        n = 0
    if Line[0] == ">" and Seq != "":
        SeqLen = len(Seq)
        n = n + 1
        print "chr\t-\t" + SeqID + "\t" + str(n) + "\t" + str(0) + "\t" + str(SeqLen) + "\t" + "chr" + str(n)
        Seq = ""
        SeqID = Line.split('>')[1]
    if Line[0] != ">":
        Seq = Seq + Line
print "chr\t-\t" + SeqID + "\t" + str(n) + "\t" + str(0) + "\t" + str(SeqLen) + "\t" + "chr" + str(n)
