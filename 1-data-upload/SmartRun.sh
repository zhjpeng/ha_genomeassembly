minimap2 -ax sr ../../20200205/a0-ref_genome/HaPG.fa HaDNA_1.fq.gz HaDNA_2.fq.gz -t 48 > Illuminan.sam
samtools view -Sb Illuminan.sam > Illuminan.bam

minimap2 -ax map-pb ../../20200205/a0-ref_genome/HaPG.fa ./pacbio.fa.gz -t 48 > Pacbio.sam
samtools view -Sb Pacbio.sam > Pacbio.bam

## upload data to NCBI
ascp -i /home/newsdd/20200216/c1-genomeAssembly/a1-rawdata-pacbio-illumina/aspera.openssh -QT -1100m -k1 -d /home/newsdd/20200216/c1-genomeAssembly/a1-rawdata-pacbio-illumina/1-raw-data-upload-ncbi